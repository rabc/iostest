## Berlin Phil Media – iOS Developer Challenge

* The thumbnail download was replaced by `URLSession` inside an extension for the `UIImageView`, so it can be downloaded asynchronously
* The Picture in Picture should be initiated by the user first using the new icon in the video screen