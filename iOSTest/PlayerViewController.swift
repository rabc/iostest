import AVFoundation
import UIKit
import AVKit

final class PlayerViewController: UIViewController {

    @IBOutlet private weak var playerView: PlayerView!

    @IBOutlet private weak var statusLabel: UILabel!

    @IBOutlet private weak var playPauseButton: UIButton! {
        didSet {
            playPauseButton.setImage(UIImage(systemName: "playpause.fill"), for: .normal)
        }
    }
    
    @IBOutlet private weak var pipButton: UIButton! {
        didSet {
            pipButton.setImage(AVPictureInPictureController.pictureInPictureButtonStartImage, for: .normal)
            pipButton.setImage(AVPictureInPictureController.pictureInPictureButtonStopImage, for: .selected)
        }
    }

    weak var playerController: PlayerController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(appDidEnterBackground),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
    }

    deinit {
        playerController?.detachPlayerLayer(playerView.playerLayer)
    }
    
    @objc func appWillEnterForeground() {
        playerController?.attachPlayerLayer(playerView.playerLayer)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func appDidEnterBackground() {
        playerController?.detachPlayerLayer(playerView.playerLayer)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(appWillEnterForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }

    @IBAction func playPause(_ sender: UIButton) {
        playerController?.playPause()
    }
    
    @IBAction func startPip(_ sender: UIButton) {
        if let playerController = playerController {
            pipButton.isSelected = playerController.startStopPip()
        }
    }
}

extension PlayerViewController: PlayerControllerDelegate {

    func playbackStarted() {
        statusLabel.text = nil
    }

    func playbackFinished() {
        statusLabel.text = "No Video"
    }
    
    func pipFinished() {
        pipButton.isSelected = false
    }
}

extension PlayerViewController: PlayerControlProtocol {
    func setupPlayerController(_ controller: PlayerController) {
        playerController = controller
        playerController?.delegate = self
        playerController?.attachPlayerLayer(playerView.playerLayer)
        
        pipButton.isHidden = !controller.isPiPSupported
    }
}
