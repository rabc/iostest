import AVFoundation
import Foundation

final class Video: Decodable {

    private enum CodingKeys: String, CodingKey {
        case title
        case hdQuality = "hd"
        case thumbnailURL = "thumb"
        case videoURL = "source"
    }

    var title: String
    var hdQuality: Bool
    var thumbnailURL: URL
    var videoURL: URL

    lazy var playerItem: AVPlayerItem = {
        AVPlayerItem(url: videoURL)
    }()
}
