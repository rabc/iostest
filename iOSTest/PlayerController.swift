import AVFoundation
import AVKit

protocol PlayerControllerDelegate: AnyObject {
    func playbackStarted()
    func playbackFinished()
    func pipFinished()
}

final class PlayerController: NSObject {

    weak var delegate: PlayerControllerDelegate?

    private lazy var player = AVPlayer()
    
    private var pictureInPictureController: AVPictureInPictureController?
    
    var isPiPSupported: Bool { AVPictureInPictureController.isPictureInPictureSupported() }
    var isVideoPlaying: Bool { player.timeControlStatus == .playing }
    var isPictureInPictureActive: Bool { pictureInPictureController?.isPictureInPictureActive ?? false }
    
    private var observerToken: NSObjectProtocol?
    
    override init() {
        try? AVAudioSession.sharedInstance().setCategory(.playback)
        
        super.init()
    }
    
    func attachPlayerLayer(_ playerLayer: AVPlayerLayer) {
        playerLayer.player = player
        
        if isPiPSupported && pictureInPictureController == nil {
            pictureInPictureController = AVPictureInPictureController(playerLayer: playerLayer)
            pictureInPictureController?.delegate = self
        }
    }

    func detachPlayerLayer(_ playerLayer: AVPlayerLayer) {
        guard !isPictureInPictureActive else { return }
        pictureInPictureController = nil
        playerLayer.player = nil
    }

    func play(_ video: Video) {
        removeObserver()
        
        player.replaceCurrentItem(with: video.playerItem)
        player.seek(to: .zero)
        player.play()

        delegate?.playbackStarted()
        activateAudioSession(true)
        
        observerToken = NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime,
                                                               object: video.playerItem,
                                                               queue: nil) { [weak self] _ in
            
            self?.activateAudioSession(false)
            self?.delegate?.playbackFinished()
            self?.removeObserver()
        }
    }
    
    private func removeObserver() {
        if let token = observerToken {
            NotificationCenter.default.removeObserver(token)
        }
    }

    func playPause() {
        if player.rate == 1 {
            activateAudioSession(false)
            player.pause()
        } else {
            activateAudioSession(true)
            player.play()
        }
    }
    
    func startStopPip() -> Bool {
        if isVideoPlaying && !isPictureInPictureActive {
            pictureInPictureController?.startPictureInPicture()
            return true
        } else {
            pictureInPictureController?.stopPictureInPicture()
            return false
        }
    }
    
    private func activateAudioSession(_ status: Bool) {
        try? AVAudioSession.sharedInstance().setActive(status, options: .notifyOthersOnDeactivation)
    }
}

extension PlayerController: AVPictureInPictureControllerDelegate {
    func pictureInPictureControllerDidStopPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        delegate?.pipFinished()
    }
}
