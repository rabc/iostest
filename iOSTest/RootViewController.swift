import UIKit

final class RootViewController: UIViewController {
    weak var playerController: PlayerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let playerController = playerController {
            self.children
                .compactMap { $0 as? PlayerControlProtocol }
                .forEach { $0.setupPlayerController(playerController) }
        }
    }
}
