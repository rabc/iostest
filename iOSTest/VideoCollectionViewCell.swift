import UIKit

final class VideoCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var thumbnailImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var hdImageView: UIImageView!

    var video: Video? {
        didSet {
            guard let video = video else {
                return
            }
            
            thumbnailImageView.download(image: video.thumbnailURL)
            titleLabel.text = video.title
            hdImageView.isHidden = !video.hdQuality
        }
    }

    override func prepareForReuse() {
        video = nil
    }
}

extension UIImageView {
    func download(image: URL) {
        let request = URLRequest(url: image, cachePolicy: .returnCacheDataElseLoad)
        let task = URLSession.shared.dataTask(with: request) { (data, _, _) in
            guard let data = data else { return }
            DispatchQueue.main.async {
                self.image = UIImage(data: data)
            }
        }
        
        task.resume()
    }
}
