import XCTest
import AVFoundation
@testable import iOSTest

class PlayerControllerTests: XCTestCase {
    
    lazy var videos: [Video] = {
        guard
            let path = Bundle.main.path(forResource: "data", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)),
            let videoData = try? JSONDecoder().decode(VideoData.self, from: data)
        else {
            assertionFailure("failed to load data")
            return []
        }
        return videoData.videos.sorted { $0.title < $1.title }
    }()
    
    func testAttachLayer() {
        let controller = PlayerController()
        
        let layer = AVPlayerLayer()
        controller.attachPlayerLayer(layer)
        
        XCTAssertNotNil(layer.player)
    }
    
    func testDetachLayer() {
        let controller = PlayerController()
        
        let layer = AVPlayerLayer()
        layer.player = AVPlayer()
        controller.detachPlayerLayer(layer)
        
        XCTAssertNil(layer.player)
    }
    
    func testPlay() {
        let controller = PlayerController()
        
        let delegate = MockDelegate()
        controller.delegate = delegate
        
        let layer = AVPlayerLayer()
        
        controller.attachPlayerLayer(layer)
        
        controller.play(videos.first!)
        XCTAssertTrue(delegate.playbackStartedCalled)
        XCTAssertNotNil(layer.player?.currentItem)
        
        NotificationCenter.default.post(name: .AVPlayerItemDidPlayToEndTime, object: layer.player?.currentItem)
        
        XCTAssertTrue(delegate.playbackFinishedCalled)
    }
    
    func testPlayPause() {
        let controller = PlayerController()
        
        let delegate = MockDelegate()
        controller.delegate = delegate
        
        let layer = AVPlayerLayer()
        
        controller.attachPlayerLayer(layer)
        
        controller.play(videos.first!)
        
        controller.playPause()
        XCTAssertEqual(layer.player!.timeControlStatus, .paused)
        
        controller.playPause()
        XCTAssertNotEqual(layer.player!.timeControlStatus, .paused)
    }

}

class MockDelegate: PlayerControllerDelegate {
    var playbackStartedCalled = false
    func playbackStarted() {
        playbackStartedCalled = true
    }
    
    var playbackFinishedCalled = false
    func playbackFinished() {
        playbackFinishedCalled = true
    }
    
    var pipFinishedCalled = false
    func pipFinished() {
        pipFinishedCalled = true
    }
}

class MockAudioSession: AVAudioSession {
    var activeStatus: Bool = false
    override func setActive(_ active: Bool, options: AVAudioSession.SetActiveOptions = []) throws {
        activeStatus = active
    }
}
